const _ = require(`lodash`)
const Promise = require(`bluebird`)
const path = require(`path`)
const slash = require(`slash`)
const DEPLOY_ENV = process.env.DEPLOY_ENV || 'lbn_published_production'

/**
 * Generate node edges
 *
 * @param {any} { node, actions, getNode }
 */
exports.onCreateNode = ({ node, actions }) => {
  const { createNodeField } = actions

  if (!Object.prototype.hasOwnProperty.call(node, 'meta')) {
    return
  }

  let deploy

  if (node.meta[DEPLOY_ENV]) {
    deploy = true
  } else {
    deploy = false
  }

  createNodeField({ node, name: 'deploy', value: deploy })
}
// Implement the Gatsby API “createPages”. This is
// called after the Gatsby bootstrap is finished so you have
// access to any information necessary to programmatically
// create pages.
// Will create pages for WordPress pages (route : /{slug})
// Will create pages for WordPress posts (route : /post/{slug})
exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  return new Promise((resolve, reject) => {
    const homePage = path.resolve('./src/pages/index.js')
    createPage({
      // Each page is required to have a `path` as well
      // as a template component. The `context` is
      // optional but is often necessary so the template
      // can query data specific to each page.
      path: `/`,
      component: slash(homePage),
    })
    resolve()
  })
}
