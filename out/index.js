'use strict';
var functions = require('firebase-functions');
var https = require('https');
var cors = require('cors')({
  origin: true });


exports.deploy = functions.https.onRequest(function (client_req, client_res) {
  return cors(client_req, client_res, function () {
    console.log('serve: ' + client_req.url);
    var options = {
      hostname: 'jujube.wpengine.com',
      path: client_req.url,
      method: client_req.method,
      auth: 'demo:000000' };


    var proxy = https.request(options, function (res) {
      client_res.writeHead(res.statusCode, res.headers);
      return res.pipe(
      client_res,
      {
        end: true });


    });

    client_req.pipe(
    proxy,
    {
      end: true });


  });
});