'use strict';Object.defineProperty(exports, "__esModule", { value: true });var _keys = require('babel-runtime/core-js/object/keys');var _keys2 = _interopRequireDefault(_keys);var _stringify = require('babel-runtime/core-js/json/stringify');var _stringify2 = _interopRequireDefault(_stringify);var _promise = require('babel-runtime/core-js/promise');var _promise2 = _interopRequireDefault(_promise);exports.



makeRequest = makeRequest;exports.

























































getQueryString = getQueryString;exports.







wrapAsync = wrapAsync;var _nodeFetch = require('node-fetch');var _nodeFetch2 = _interopRequireDefault(_nodeFetch);var _btoa = require('btoa');var _btoa2 = _interopRequireDefault(_btoa);var _qs = require('qs');var _qs2 = _interopRequireDefault(_qs);function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}function makeRequest(url, data) {var method = void 0,headers = {};if (!data) method = 'GET';else {method = 'POST';headers['Content-Type'] = 'application/json';}var self = { send: function send() {return new _promise2.default(function (resolve, reject) {if (method === 'GET' && data) {url += '?' + getQueryString(data);}var option = { method: method, credentials: 'include', mode: 'cors', cache: 'default', headers: headers };if (method === 'POST' && data) {if (headers['Content-Type'].includes('x-www-form-urlencoded')) {option.body = _qs2.default.stringify(data);} else if (headers['Content-Type'].includes('application/json')) {option.body = (0, _stringify2.default)(data);}}(0, _nodeFetch2.default)(url, option).then(function (response) {return response.text();}).then(function (body) {var result = body;try {result = JSON.parse(body);} catch (e) {}return resolve(result);}).catch(function (error) {return reject(error);});});}, addHeader: function addHeader(key, value) {if (value) headers[key] = value;return self;}, setMethod: function setMethod(type) {method = type;return self;}, setAuth: function setAuth(username) {var password = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';headers['Authorization'] = 'Basic ' + (0, _btoa2.default)(username + ':' + password);return self;} };return self;}function getQueryString(query) {var esc = encodeURIComponent;var queryString = (0, _keys2.default)(query).map(function (k) {return esc(k) + '=' + esc(query[k]);}).join('&');return queryString;}function wrapAsync(fn) {
  return function (req, res, next) {
    // Make sure to `.catch()` any errors and pass them along to the `next()`
    // middleware in the chain, in this case the error handler.
    fn(req, res, next).catch(function (e) {
      next(e);
    });
  };
}