module.exports = {
    "parser": "babel-eslint",
    "env": {
        "browser": true,
        "node": true,
        "es6": true,
        "jest": true,
        "cypress/globals": true
    },
    "plugins": [
        "react",
        "cypress"
    ],
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended"
    ],
    "rules": {
        'no-console': ["off"],
        'require-await': ["error"],
        'no-empty': ["off"],
        "indent": ["error", 2, { "SwitchCase": 1 }]
    }
}