import fetch from 'node-fetch';
import btoa from 'btoa';
import qs from 'qs';

export function makeRequest(url, data) {
  let method, headers = {};
  if (!data) method = 'GET';
  else {
    method = 'POST';
    headers['Content-Type'] = 'application/json';
  }
  var self = {
    send: () => {
      return new Promise((resolve, reject) => {
        if (method === 'GET' && data) {
          url += '?' + getQueryString(data);
        }
        var option = {
          method: method,
          credentials: 'include',
          mode: 'cors',
          cache: 'default',
          headers: headers
        }
        if (method === 'POST' && data) {
          if (headers['Content-Type'].includes('x-www-form-urlencoded')) {
            option.body = qs.stringify(data);
          }
          else if (headers['Content-Type'].includes('application/json')) {
            option.body = JSON.stringify(data);
          }
        }
        fetch(url, option).then((response) => {
          return response.text();
        }).then((body) => {
          let result = body;
          try {
            result = JSON.parse(body);
          } catch (e) {}
          return resolve(result);
        })
          .catch((error) => {
            return reject(error);
          })
      })
    },
    addHeader: (key, value) => {
      if (value) headers[key] = value;
      return self;
    },
    setMethod: (type) => {
      method = type;
      return self;
    },
    setAuth: (username, password = '') => {
      headers['Authorization'] = 'Basic ' + btoa(`${username}:${password}`);
      return self;
    }
  }
  return self;
}

export function getQueryString(query) {
  let esc = encodeURIComponent
  let queryString = Object.keys(query)
    .map(k => esc(k) + '=' + esc(query[k]))
    .join('&');
  return queryString;
}

export function wrapAsync(fn) {
  return function(req, res, next) {
    // Make sure to `.catch()` any errors and pass them along to the `next()`
    // middleware in the chain, in this case the error handler.
    fn(req, res, next).catch((e) => {
        next(e);
    });
  };
}