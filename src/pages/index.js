import React from 'react'
import MediaQuery from 'react-responsive'
import { DEVICE_QUERIES } from '../constants'
import { graphql } from 'gatsby'

//components
import HomeDesktop from '../components/Desktop/Home'
import HomeMobile from '../components/Mobile/Home'

class HomePage extends React.Component {
  constructor() {
    super()
    this.state = {
      scrollPosition: 0,
    }
    this.setScrollPosition = this.setScrollPosition.bind(this)
  }
  setScrollPosition(scrollPosition) {
    this.setState({ scrollPosition })
  }
  render() {
    const { data } = this.props
    return (
      <div>
        <MediaQuery query={DEVICE_QUERIES.desktop}>
          <HomeDesktop
            setScrollPosition={this.setScrollPosition}
            scrollPosition={this.state.scrollPosition}
            data={data}
          />
        </MediaQuery>
        <MediaQuery query={DEVICE_QUERIES.mobile}>
          <HomeMobile
            setScrollPosition={this.setScrollPosition}
            scrollPosition={this.state.scrollPosition}
            data={data}
          />
        </MediaQuery>
      </div>
    )
  }
}

export const query = graphql`
  fragment imageGetter on wordpress__wp_media {
    source_url
    localFile {
      childImageSharp {
        fluid(quality: 100, maxWidth: 2000) {
          ...GatsbyImageSharpFluid
        }
        fixed(quality: 100) {
          ...GatsbyImageSharpFixed
        }
      }
      publicURL
    }
  }
  query {
    #hero data
    hero_data: allWordpressPage {
      edges {
        node {
          acf {
            hero_background_image_desktop {
              ...imageGetter
            }
            hero_background_image_mobile {
              localFile {
                publicURL
              }
            }
          }
        }
      }
    }

    # section1 data
    section1_data: allWordpressPage {
      edges {
        node {
          acf {
            section1_title
            section1_body
          }
        }
      }
    }

    #section2
    section2_data: allWordpressPage {
      edges {
        node {
          acf {
            section2_image {
              ...imageGetter
            }
            section2_body
            section2_title
            section2_detail {
              detail_x_position
              detail_y_position
              detail_body
              detail_title
            }
          }
        }
      }
    }

    #section3
    section3_data: allWordpressPage {
      edges {
        node {
          acf {
            section3_title
            section3_detail {
              body
              image {
                ...imageGetter
              }
              title
            }
          }
        }
      }
    }

    #section4
    section4_data: allWordpressPage {
      edges {
        node {
          acf {
            section4_body
            section4_title
            section4_image {
              localFile {
                publicURL
              }
            }
          }
        }
      }
    }

    #section5
    section5_data: allWordpressPage {
      edges {
        node {
          acf {
            section5_body
            section5_title
            section5_detail {
              image {
                localFile {
                  publicURL
                }
              }
              title
            }
          }
        }
      }
    }

    #section6
    section6_data: allWordpressPage {
      edges {
        node {
          acf {
            section6_carousel {
              title
              body
              image {
                ...imageGetter
              }
            }
          }
        }
      }
    }

    #section7
    section7_data: allWordpressPage {
      edges {
        node {
          acf {
            section7_reviews {
              image {
                ...imageGetter
              }
              name
              text
            }
          }
        }
      }
    }

    #section8
    section8_data: allWordpressPage {
      edges {
        node {
          acf {
            section8_title
            section8_text
          }
        }
      }
    }

    #section9
    section9_data: allWordpressPage {
      edges {
        node {
          acf {
            section9_video_desktop {
              ...imageGetter
            }
            section9_video_mobile {
              ...imageGetter
            }
            section9_button_alignment
            section9_button_label
            section9_text
            section9_title
          }
        }
      }
    }

    #section10
    section10_data: allWordpressPage {
      edges {
        node {
          acf {
            section10_body
            section10_title
            section10_products {
              image {
                ...imageGetter
              }
              title
              subtitle
              price
              link
            }
          }
        }
      }
    }

    #footer data
    footer_data: allWordpressPage {
      edges {
        node {
          acf {
            footer_subscribe {
              title
              input_placeholder
              button_text
            }
            footer_contact_support {
              title
              phone_number
              fax
              name
              email
              time
            }
            footer_browse_the_site {
              links {
                name
                path
              }
              title
            }
            footer_copyright_notice
            footer_shop_best_sellers {
              title
              links {
                name
                path
              }
            }
          }
        }
      }
    }

    #wp menus data
    menu_data: allWordpressWpApiMenusMenusItems(
      filter: { items: { elemMatch: { object_slug: { eq: "shop-bags" } } } }
    ) {
      edges {
        node {
          id
          items {
            url
            title
          }
        }
      }
    }

    #site data
    site {
      siteMetadata {
        title
      }
    }
  }
`

export default HomePage
