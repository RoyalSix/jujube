export const DEVICE_QUERIES = {
  desktop: "(min-width: 700px)",
  mobile: "(max-width: 699px)",
  portrait: "(orientation: portrait)",
  landscape: "(orientation: landscape)",
  retina: "(min-resolution: 2dppx)"
}
