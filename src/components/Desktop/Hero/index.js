import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import Video from './Video'
import { withStyles } from '@material-ui/core'
import Image from '../../Common/Image'
import { play_button, video_close_icon } from 'images'

const styles = {
  root: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },

  paper: {
    backgroundColor: 'transparent',
    boxShadow: 'none',
    overflow: 'hidden',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
  },
}

class Hero extends React.Component {
  constructor() {
    super()
    this.state = {
      open: false,
    }
  }
  render() {
    const { data, classes } = this.props
    const { hero_background_image_desktop } = data
    return (
      <div
        style={{
          width: '100vw',
          position: 'relative',
        }}
      >
        <Dialog
          BackdropProps={{
            classes: {
              root: classes.root,
            },
          }}
          PaperProps={{
            classes: {
              root: classes.paper,
            },
          }}
          fullWidth={true}
          maxWidth={false}
          open={this.state.open}
          onClose={() => this.setState({ open: false })}
        >
          <img
            onClick={() =>
              this.setState({
                open: false,
              })
            }
            style={{
              cursor: 'pointer',
              zIndex: 1,
              position: 'absolute',
              top: '3%',
              right: '6%',
              height: '5vh',
            }}
            src={video_close_icon}
          />
          <div
            style={{
              width: '86%',
              height: '90%',
            }}
          >
            <Video />
          </div>
        </Dialog>
        <div
          onClick={() =>
            this.setState({
              open: true,
            })
          }
          style={{
            position: 'relative',
            cursor: 'pointer',
          }}
        >
          <img
            style={{
              filter: 'drop-shadow(0px 0px 12px rgba(0,0,0,0.20))',
              zIndex: 1,
              height: '9vw',
              minHeight: 120,
              position: 'absolute',
              top: '50%',
              left: '50%',
              transform: 'translate(-50%, -50%)',
            }}
            src={play_button}
          />
          <Image
            {...hero_background_image_desktop}
            style={{
              width: '100%',
              height: 'auto',
              outline: 'none',
              objectFit: 'contain',
            }}
          />
        </div>
      </div>
    )
  }
}
export default withStyles(styles)(Hero)
