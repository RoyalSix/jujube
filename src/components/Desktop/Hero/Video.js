import React from 'react'

class Video extends React.Component {
  componentDidMount() {
    new Plyr('#hero_player', {
      autoplay: true,
      muted: false,
    })
  }
  render() {
    return (
      <div
        id="hero_player"
        data-plyr-provider="vimeo"
        data-plyr-embed-id="76979871"
      />
    )
  }
}

export default Video
