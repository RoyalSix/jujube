import React from 'react'
import Detail from './Detail.js'

const Section5 = ({ data }) => {
  const { section5_body, section5_title, section5_detail } = data
  return (
    <div style={{ backgroundColor: '#FBF3F2', padding: '6% 0 5%' }}>
      <div style={{ width: '84%', margin: '0 auto' }}>
        <div className="header-text" style={{ textAlign: 'center' }}>
          {section5_title}
        </div>
        {/* underscore mark */}
        <div
          style={{
            margin: '2% auto',
            backgroundColor: '#F2AFA3',
            width: '6%',
            maxWidth: 80,
            height: 2,
          }}
        />
        <div
          className="body-text"
          style={{
            textAlign: 'center',
            margin: '0 auto',
            marginTop: '4%',
            width: '39%',
            minWidth: 400,
          }}
        >
          {section5_body}
        </div>
        <div
          style={{
            display: 'grid',
            gridTemplateColumns: `repeat(${section5_detail.length}, 1fr)`,
            marginTop: '6%',
            gridGap: '2em',
          }}
        >
          {section5_detail.map(detailData => (
            <Detail {...detailData} />
          ))}
        </div>
      </div>
    </div>
  )
}

export default Section5
