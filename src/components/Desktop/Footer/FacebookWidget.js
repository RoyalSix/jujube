import React from 'react'

const FacebookWidget = () => {
  return (
    <div
      id="facebook-widget-container"
      style={{
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        marginTop: '2%',
      }}
    >
      <a
        href="https://www.facebook.com/pg/jujubeintl/community/"
        id="facebook-widget"
      >
        <strong>
          Join The FB Community
          <i class="fas fa-external-link-alt" />
        </strong>
      </a>
      <div>
        Join over a million moms and dads in our enthusiastic, Ju-Ju-Be-loving
        community!
      </div>
    </div>
  )
}

export default FacebookWidget
