import React from 'react'

const JujubeLogo = () => {
  return (
    <div
      style={{ flex: 1, display: 'flex', justifyContent: 'center', zIndex: 1 }}
    >
      <img
        style={{
          width: '85%',
          objectFit: 'contain',
        }}
        alt="jujube"
        src="https://cdn.shopify.com/s/files/1/0012/6412/3970/files/footer-logo.png?v=1523534705"
      />
    </div>
  )
}

export default JujubeLogo
