import React from 'react'
const data = [
  {
    link: 'https://www.instagram.com/jujube_intl/',
    text: '#jujube',
  },
  {
    link: 'https://www.instagram.com/explore/tags/goodjuju/',
    text: '#goodjuju',
  },
  {
    link: 'https://www.instagram.com/explore/tags/millionmomsstrong/',
    text: '#millionmomsstrong',
  },
  {
    link: 'https://www.ju-ju-be.com/pages/military-discount',
    text: 'Military Discount',
  },
]

const Hashtags = () => {
  return (
    <div id="hashtag-container">
      {data.map(({ link, text }) => (
        <a href={link}>{text}</a>
      ))}
    </div>
  )
}

export default Hashtags
