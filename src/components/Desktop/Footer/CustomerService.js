import React from 'react'

class CustomerService extends React.Component {
  constructor() {
    super()
    this.state = {
      open: false,
    }
  }
  render() {
    return (
      <div
        id="customer-service-container"
        style={{ position: 'relative', flex: 1, zIndex: 0 }}
        className={this.state.open ? 'active' : ''}
      >
        <div className="customer-service">
          <div
            onClick={() =>
              this.setState({
                open: !this.state.open,
              })
            }
            style={{
              cursor: 'pointer',
              textTransform: 'uppercase',
              color: 'white',
              fontFamily: 'TradeGothicLTStd',
              letterSpacing: '4.5px',
              lineHeight: '44px',
              paddingLeft: 10,
              fontSize: 16,
            }}
          >
            Customer Service
          </div>
        </div>
        <div
          style={{
            width: $('#customer-service-container').width(),
          }}
          className="customer-service-nav"
        >
          <a href="https://www.ju-ju-be.com/pages/ship-return">Ship / Return</a>
          <a href="https://www.ju-ju-be.com/pages/faq">FAQ</a>
          <a href="https://www.ju-ju-be.com/pages/retail-locator">
            Retail Locator
          </a>
          <a href="https://www.ju-ju-be.com/pages/contact">Contact</a>
          <a href="https://www.ju-ju-be.com/pages/international">
            International
          </a>
          <a href="https://www.ju-ju-be.com/pages/terms-of-use">
            Terms &amp; Conditions
          </a>
        </div>
        <div
          style={{
            width: $('#customer-service-container').width(),
            height: $('.customer-service-nav').height(),
          }}
          className="customer-service-mask"
        />
      </div>
    )
  }
}

export default CustomerService
