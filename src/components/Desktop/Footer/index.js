import React from 'react'
import CustomerService from './CustomerService'
import JujubeLogo from './JujubeLogo'
import SubscribeButton from './SubscribeButton'
import FacebookWidget from './FacebookWidget'
import SocialMedia from './SocialMedia'
import Hashtags from './Hashtags'
import './footer.css'

const Footer = ({ data }) => {
  const {} = data
  return (
    <div
      id="footer"
      style={{
        backgroundColor: 'black',
      }}
    >
      <div style={{ margin: '0px 11%', paddingTop: '2%', display: 'flex' }}>
        <CustomerService />
        <JujubeLogo />
        <SubscribeButton />
      </div>
      <FacebookWidget />
      <SocialMedia />
      <Hashtags />
      <div id="footer-nav">
        <span>(c) 2018 JuJuBe Inc</span>
        <a href="https://www.ju-ju-be.com/pages/privacy-policy">
          Privacy Policy
        </a>
        <a href="https://www.ju-ju-be.com/pages/content-creators">
          Content Creators
        </a>
      </div>
    </div>
  )
}

export default Footer
