import React from 'react'

const data = [
  {
    icon: <i class="fab fa-instagram" />,
    link: 'https://www.instagram.com/jujube_intl/',
  },
  {
    icon: <i class="fab fa-facebook-f" />,
    link: 'https://www.facebook.com/jujubeintl',
  },
  {
    icon: <i class="fab fa-twitter" />,
    link: 'https://twitter.com/jujube_intl',
  },
  {
    icon: <i class="fab fa-youtube" />,
    link: 'https://www.youtube.com/user/JuJuBeTV',
  },
  {
    icon: <i class="fab fa-pinterest-p" />,
    link: 'https://www.pinterest.com/jujubebags/',
  },
]

const SocialMedia = () => {
  return (
    <div id="social-media-container">
      {data.map(({ link, icon }) => (
        <a href={link}>{icon}</a>
      ))}
    </div>
  )
}

export default SocialMedia
