import React from 'react'

const SubscribeButton = () => {
  return (
    <div id="subscribe-container">
      <input type="text" placeholder="email me exclusives &amp; deals" />
      <button>Subscribe</button>
    </div>
  )
}

export default SubscribeButton
