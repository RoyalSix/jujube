import React from 'react'

class Video extends React.Component {
  componentDidMount() {
    // Change the second argument to your options:
    // https://github.com/sampotts/plyr/#options
    const player = new Plyr('#section9_video_player', {
      fullscreen: { enabled: false, fallback: true, iosNative: false },
      autoplay: true,
      controls: [],
      muted: true,
      clickToPlay: false,
      loop: {
        active: true,
      },
    })
    player.volume = 0
    player.play()
  }
  render() {
    return (
      <div style={this.props.style}>
        <video muted id="section9_video_player" playsinline>
          <source src={this.props.src} type="video/mp4" />
        </video>
      </div>
    )
  }
}

export default Video
