import React from 'react'
import Button from '../../Common/Button'
import Video from './Video'
import { section9_background_desktop } from 'images'

const Section9 = ({ data }) => {
  const {
    section9_video_desktop,
    section9_text,
    section9_title,
    section9_button_label,
  } = data
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <Video src={section9_video_desktop.localFile.publicURL} style={{ width: '87%', margin: 'auto', marginTop: '4%' }} />
      <div
        style={{
          position: 'relative',
          display: 'flex',
          overflow: 'hidden',
          paddingBottom: '7%',
        }}
      >
        <img
          style={{
            width: '100%',
            zIndex: -1,
            position: 'absolute',
            bottom: '-51%',
            right: 0,
            width: 'auto',
            height: '52vh',
          }}
          src={section9_background_desktop}
        />
        <div
          style={{
            width: '85%',
            margin: '0 auto',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <div
            className="header-text"
            style={{
              textAlign: 'center',
              marginTop: '7%',
              textAlign: 'center',
              width: '90%',
            }}
          >
            {section9_title}
          </div>
          {/* underscore mark */}
          <div
            style={{
              margin: '1.4em 0px',
              backgroundColor: '#F2AFA3',
              width: '5%',
              maxWidth: 80,
              minHeight: 2,
            }}
          />
          <div
            className="body-text"
            style={{
              marginTop: '.9em',
              textAlign: 'center',
              width: '33%',
            }}
          >
            {section9_text}
          </div>
          <Button style={{ marginTop: '4%' }} label={section9_button_label} />
        </div>
      </div>
    </div>
  )
}

export default Section9
