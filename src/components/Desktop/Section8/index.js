import React from 'react'
import { section8_background } from 'images'

const Section8 = ({ data }) => {
  const { section8_title, section8_text } = data
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        flexDirection: 'column',
      }}
    >
      <img
        style={{
          minHeight: $('#section-8-content').height() + 30,
          width: '100%',
          top: 0,
          zIndex: -1,
        }}
        src={section8_background}
      />
      <div
        id="section-8-content"
        style={{
          width: '100%',
          margin: '0 auto',
          position: 'absolute',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <div
          className="header-text"
          style={{
            textAlign: 'center',
          }}
        >
          {section8_title}
        </div>
        {/* underscore mark */}
        <div
          style={{
            marginTop: '1.3em',
            backgroundColor: '#F2AFA3',
            width: '4.5%',
            maxWidth: 80,
            height: 2,
          }}
        />
        <div
          className="body-text"
          style={{
            minWidth: 400,
            textAlign: 'center',
            width: '28%',
            marginTop: '3%',
          }}
        >
          {section8_text}
        </div>
      </div>
    </div>
  )
}

export default Section8
