import React, { Component } from 'react'
import Hero from '../Hero'
import Layout from '../../Layout'
import Menu from '../Menu'
import Section1 from '../Section1'
import Section2 from '../Section2'
import Section3 from '../Section3'
import Section4 from '../Section4'
import Section5 from '../Section5'
import Section6 from '../Section6'
import Section7 from '../Section7'
import Section8 from '../Section8'
import Section9 from '../Section9'
import Section10 from '../Section10'
import Footer from '../Footer'
//selectors
import { DataSelector } from '../../../selectors'

export class Home extends Component {
  constructor() {
    super()
    this.state = {
      width: $(window).width(),
    }

    this.updateWidth = this.updateWidth.bind(this)
  }

  componentWillMount() {
    $(window).on('resize', this.updateWidth)
  }

  componentDidMount() {
    // $('#home-container')
    //   .delay(500)
    //   .animate({ scrollTop: this.props.scrollPosition })
  }

  componentWillUnmount() {
    this.props.setScrollPosition($('#home-container').scrollTop())
    $(window).off('resize', this.updateWidth)
  }

  updateWidth() {
    this.setState({
      width: $(window).width(),
    })
  }
  render() {
    const { data } = this.props
    const menuData = DataSelector(data).getAllDataByType('menu')[0].items
    const section1Data = DataSelector(data).getAllDataByType('section1')[0].acf
    const section2Data = DataSelector(data).getAllDataByType('section2')[0].acf
    const section3Data = DataSelector(data).getAllDataByType('section3')[0].acf
    const section4Data = DataSelector(data).getAllDataByType('section4')[0].acf
    const section5Data = DataSelector(data).getAllDataByType('section5')[0].acf
    const section6Data = DataSelector(data).getAllDataByType('section6')[0].acf
    const section7Data = DataSelector(data).getAllDataByType('section7')[0].acf
    const section8Data = DataSelector(data).getAllDataByType('section8')[0].acf
    const section9Data = DataSelector(data).getAllDataByType('section9')[0].acf
    const section10Data = DataSelector(data).getAllDataByType('section10')[0]
      .acf
    const footerData = DataSelector(data).getAllDataByType('footer')[0].acf
    const heroData = DataSelector(data).getAllDataByType('hero')[0].acf
    return (
      <Layout>
        <Menu data={menuData} />
        <div
          id="home-container"
          style={{
            height: window.innerHeight,
            overflow: 'auto',
            overflowScrolling: 'touch',
            WebkitOverflowScrolling: 'touch',
            overflowX: 'hidden',
            width: this.state.width,
            position: 'absolute',
            top: 70,
          }}
        >
          <Hero data={heroData} />
          <Section1 data={section1Data} />
          <Section2 data={section2Data} />
          <Section3 data={section3Data} />
          <Section4 data={section4Data} />
          <Section5 data={section5Data} />
          <Section6 data={section6Data} />
          <Section7 data={section7Data} />
          <Section8 data={section8Data} />
          <Section9 data={section9Data} />
          <Section10 data={section10Data} />
          <Footer data={footerData} />
        </div>
      </Layout>
    )
  }
}

export default Home
