import React from 'react'
import { jujube_logo } from 'images'
import ExpandedMenu from './ExpandedMenu'
import './Menu.css'
import Button from '../../Common/Button'

class Menu extends React.Component {
  constructor() {
    super()
    this.state = {
      open: false,
    }
  }
  render() {
    const { data } = this.props
    return (
      <div
        style={{
          zIndex: 100,
          width: '100vw',
          position: 'fixed',
          backgroundColor: 'white',
        }}
      >
        <div
          style={{
            margin: '0 7%',
            height: 70,
            backgroundColor: 'white',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <img
            style={{ cursor: 'pointer' }}
            onClick={() => (window.location.pathname = '/')}
            alt="jujube"
            src={jujube_logo}
          />
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            {data.map(({ title, url }) => (
              <Button
                href={url}
                label={title}
                style={{
                  height: 40,
                  width: 115,
                  marginRight: '4vw',
                }}
              />
            ))}
            <div
              style={{
                display: 'flex',
                cursor: 'pointer',
                padding: '0 4%',
              }}
              onClick={() => this.setState({ open: !this.state.open })}
            >
              <div
                className={this.state.open ? 'open' : ''}
                id="hamburger-icon"
              >
                <span />
                <span />
                <span />
                <span />
              </div>
            </div>
          </div>
        </div>
        <ExpandedMenu open={this.state.open} />
      </div>
    )
  }
}

export default Menu
