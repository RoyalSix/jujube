import React from 'react'
import { menu_expanded_background } from 'images'
import './Menu.css'
const menu_options = [
  {
    link: 'https://ju-ju-be.com/collections/new',
    name: 'New Releases',
  },
  {
    link: 'https://ju-ju-be.com/collections/bags',
    name: 'All Bags',
  },
  {
    link: 'https://ju-ju-be.com/collections/accessories',
    name: 'Accessories for Mom',
  },
  {
    link: 'https://ju-ju-be.com/collections/snack-bags',
    name: 'Accessories for Kids',
  },
  {
    link: 'https://ju-ju-be.com/collections/trending-products',
    name: 'Shop Style',
  },
  {
    link: 'https://ju-ju-be.com/collections/tokidoki-x-ju-ju-be',
    name: 'Collaborations',
  },
  {
    link: 'https://ju-ju-be.com/collections/sale',
    name: 'JJB Rack',
  },
]
class ExpandedMenu extends React.Component {
  render() {
    return (
      <div
        id="menu-options"
        style={{
          zIndex: 1,
          width: window.innerWidth,
          height: 'calc(100vh - 70px)',
          backgroundColor: 'white',
          position: 'absolute',
          opacity: this.props.open ? 1 : 0,
          transition: '.4s all',
          display: 'flex',
          visibility: this.props.open ? 'visible' : 'hidden',
          flexDirection: 'column',
          justifyContent: 'center',
        }}
      >
        <img
          style={{ position: 'absolute', bottom: 0, right: 0 }}
          src={menu_expanded_background}
        />
        {menu_options.map(({ link, name }) => (
          <a
            style={{
              outline: 'none',
              textDecoration: 'none',
              color: 'black',
              margin: '0 auto',
              marginBottom: '4%',
              fontSize: 24,
              textAlign: 'center',
              fontFamily: 'PlayfairDisplay',
              fontWeight: 'bold',
            }}
            href={link}
          >
            {name}
          </a>
        ))}
      </div>
    )
  }
}

export default ExpandedMenu
