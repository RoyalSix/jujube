import React from 'react'
import BackPack from './BackPack'
import Title from './Title'
import Body from './Body'

const Section2 = ({ data }) => {
  return (
    <div
      style={{
        backgroundColor: '#FBF3F2',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        paddingBottom: '9%',
      }}
    >
      <Title {...data} />
      <Body {...data} />
      <BackPack {...data} />
    </div>
  )
}

export default Section2
