import React from 'react'
const title = 'Made With Mom In Mind'

const Title = ({ section2_title }) => {
  return (
    <div
      className="header-text"
      style={{
        textAlign: 'center',
        padding: '5% 0 0 0',
        margin: '0 10%',
      }}
    >
      {section2_title}
    </div>
  )
}

export default Title
