import React from 'react'

const Body = ({ section2_body }) => {
  return (
    <div
      className="body-text"
      style={{
        textAlign: 'center',
        margin:'4% auto 4%',
        width: '39%',
      }}
    >
      {section2_body}
    </div>
  )
}

export default Body
