import React from 'react'
import { backpack_add_icon } from 'images'

const BackPackPlusIcon = ({ x, y, body, selected, select, index, title }) => {
  const transformStyle = selected
    ? {
        transform: 'rotate(135deg)',
      }
    : {}

  const textBoxStyle = selected
    ? {
        opacity: 1,
        visibility: 'visible',
      }
    : {
        opacity: 0,
        visibility: 'hidden',
      }
  return (
    <div
      style={{
        height: 50,
        width: 50,
        position: 'absolute',
        top: `${y}%`,
        left: `${x}%`,
        display: 'flex',
        cursor: 'pointer',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: selected ? 1 : 0,
      }}
    >
      <img
        onClick={select}
        style={{
          zIndex: selected ? 2 : 0,
          objectFit: 'cover',
          marginLeft: '.3em',
          marginTop: '.2em',
          transition: 'transform .1s ease-in-out',
          ...transformStyle,
        }}
        src={backpack_add_icon}
      />
      <div
        id={`backpack-feature-text-${index}`}
        style={{
          backgroundColor: 'white',
          borderRadius: 10,
          height: 220,
          width: 290,
          position: 'absolute',
          transition: 'opacity .15s ease-in',
          transitionDelay: '.1s',
          top: '50%',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          ...textBoxStyle,
          zIndex: 1,
        }}
      >
        <div
          style={{
            textAlign: 'center',
            fontSize: 21,
            letterSpacing: 0,
            color: 'black',
            fontFamily: 'PlayfairDisplay',
            fontWeight: 'bold',
          }}
        >
          {title}
        </div>
        <div
          className="body-text"
          style={{
            textAlign: 'center',
            margin: '0 5%',
          }}
        >
          {body}
        </div>
      </div>
    </div>
  )
}

export default BackPackPlusIcon
