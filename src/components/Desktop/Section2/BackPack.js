import React from 'react'
import BackPackPlusIcon from './BackPackPlusIcon'
import Image from '../../Common/Image'
const percent_adjustments = [[3, -9], [0, 0], [2, 16], [2, -7]]

class BackPack extends React.Component {
  constructor() {
    super()
    this.state = {
      selected: null,
      closedByMouseUp: false,
      lastIndex: null,
    }
    this.showFeature = this.showFeature.bind(this)
  }

  componentDidMount() {
    window.addEventListener('mouseup', () => {
      if (this.state.selected !== null) {
        this.setState({
          selected: null,
          closedByMouseUp: true,
          lastIndex: this.state.selected,
        })
      }
    })
  }

  showFeature(index, notShowing) {
    if (
      (notShowing && !this.state.closedByMouseUp) ||
      (this.state.lastIndex !== null && index != this.state.lastIndex)
    ) {
      this.setState(
        {
          selected: index,
        },
        () => {
          var $window = $(window),
            $element = $(`#backpack-feature-text-${index}`),
            elementTop = $element.offset().top,
            elementHeight = $element.height(),
            viewportHeight = $window.height(),
            scrollIt = elementTop - (viewportHeight - elementHeight) / 2
          $('html, body').animate({ scrollTop: scrollIt }, 400)
        }
      )
    }
    this.setState({ closedByMouseUp: false })
  }

  render() {
    const { section2_image, section2_detail } = this.props
    return (
      <div
        style={{
          position: 'relative',
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Image
          {...section2_image}
          style={{
            width: '31.8vw',
            height: '100%',
          }}
          imgStyle={{
            width: '100%',
            height: '100%',
          }}
        />
        {section2_detail.map(
          (
            { detail_x_position, detail_y_position, detail_body, detail_title },
            index
          ) => (
            <BackPackPlusIcon
              index={index}
              select={() => {
                this.showFeature(index, this.state.selected === null)
              }}
              selected={this.state.selected === index}
              x={
                parseInt(detail_x_position) +
                parseInt(percent_adjustments[index][1])
              }
              y={
                parseInt(detail_y_position) +
                parseInt(percent_adjustments[index][0])
              }
              body={detail_body}
              title={detail_title}
            />
          )
        )}
      </div>
    )
  }
}

export default BackPack
