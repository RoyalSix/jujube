import React from 'react'
import Image from '../../Common/Image'
import Button from '../../Common/Button'
import './section10.css'

const Section10 = ({ data }) => {
  const { section10_title, section10_products, section10_body } = data
  return (
    <div>
      <div>
        <div
          className="header-text"
          style={{
            margin: '0 auto',
            marginTop: '6%',
            textAlign: 'center',
            width: '90%',
          }}
        >
          {section10_title}
        </div>
        {/* underscore mark */}
        <div
          style={{
            margin: '1.4em auto',
            backgroundColor: '#F2AFA3',
            width: '5%',
            maxWidth: 80,
            minHeight: 2,
          }}
        />
        <div
          className="body-text"
          style={{
            margin: '0 auto',
            textAlign: 'center',
            width: '33%',
          }}
        >
          {section10_body}
        </div>
      </div>
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: `repeat(${section10_products.length}, 1fr)`,
          padding: '7%',
          paddingTop: '3%',
          gridGap: '0 2%',
          position: 'relative',
          overflowY: 'hidden',
        }}
      >
        {section10_products.map(({ image, title, subtitle, price, link }) => {
          return (
            <a
              href={link}
              id="product-container"
              style={{
                WebkitAppearance: 'none',
                outline: 'none',
                textDecoration: 'none',
                transition: '.3s all ease-in-out',
                backgroundColor: 'white',
                display: 'flex',
                flexDirection: 'column',
              }}
            >
              <Image {...image} style={{ width: '100%' }} />
              <div style={{ margin: '0 7%' }}>
                {/* underscore mark */}
                <div
                  style={{
                    marginTop: '7%',
                    backgroundColor: '#F2AFA3',
                    width: '15%',
                    maxWidth: 80,
                    height: 2,
                  }}
                />
                <div
                  style={{
                    marginTop: '6%',
                    fontSize: 20,
                    color: 'black',
                    lineHeight: '27px',
                    letterSpacing: 0,
                    fontFamily: 'PlayfairDisplay',
                    fontWeight: 'bold',
                  }}
                >
                  <span>{title}</span>
                  <br />
                  <span style={{ color: '#F2AFA3' }}>{subtitle}</span>
                </div>
                <div
                  style={{
                    fontSize: 18,
                    color: 'black',
                    lineHeight: '23px',
                    letterSpacing: 0,
                    margin: '3% 0',
                    fontWeight: 400,
                  }}
                >
                  ${price}
                </div>
                <Button style={{ marginBottom: '7%' }} label={'SHOP NOW'} />
              </div>
            </a>
          )
        })}
      </div>
    </div>
  )
}

export default Section10
