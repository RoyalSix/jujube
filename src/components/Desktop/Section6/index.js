import React from 'react'
import Image from '../../Common/Image'

class Section6 extends React.Component {
  constructor() {
    super()
    this.state = {
      active: 0,
    }
  }
  render() {
    const {
      data: { section6_carousel },
    } = this.props
    const active = this.state.active
    return (
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: `repeat(${section6_carousel.length}, 1fr)`,
          gridGap: '0 2px',
          marginTop: 4,
        }}
      >
        {section6_carousel.map(({ image, title, body }, index) => {
          return (
            <div
              onMouseEnter={() => this.setState({ active: index })}
              style={{
                transition: '.3s all ease-in-out',
                cursor: 'pointer',
                position: 'relative',
                opacity: active === index ? 1 : 0.25,
              }}
            >
              <Image {...image} style={{ width: '33.5vw', height: '70vh' }} />
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  textAlign: 'left',
                  position: 'absolute',
                  bottom: 0,
                  margin: '9% 6%',
                  display: active === index ? 'block' : 'none',
                }}
              >
                <div
                  style={{
                    fontSize: 33,
                    color: 'white',
                    letterSpacing: 0,
                    fontFamily: 'PlayfairDisplay',
                    fontWeight: 'bold',
                  }}
                >
                  {title}
                </div>
                {/* underscore mark */}
                <div
                  style={{
                    backgroundColor: '#F2AFA3',
                    width: '15%',
                    maxWidth: 80,
                    height: 2,
                    margin: '5% 0px 8%',
                    alignSelf: 'flex-start',
                  }}
                />
                <div
                  style={{
                    fontSize: 15,
                    lineHeight: '23px',
                    letterSpacing: 0,
                    color: 'white',
                    fontWeight: 400,
                  }}
                >
                  {body}
                </div>
              </div>
            </div>
          )
        })}
      </div>
    )
  }
}

export default Section6
