import React from 'react'
import { section1_paint_mark } from 'images'

const Section1 = ({ data }) => {
  const { section1_title } = data
  return (
    <div
      style={{
        position: 'relative',
        zIndex: -1,
        display: 'flex',
      }}
    >
      <img
        alt="mark"
        src={section1_paint_mark}
        style={{
          opacity: 0.5,
          height: '70%',
          position: 'absolute',
        }}
      />
      <div
        style={{
          margin: '7% 7% 5%',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <div
          className="header-text"
          style={{
            letterSpacing: '0',
            marginBottom: '2%',
          }}
        >
          {section1_title}
        </div>
        {/* underscore mark */}
        <div
          style={{
            backgroundColor: '#F2AFA3',
            width: '15%',
            maxWidth: 80,
            minHeight: 2,
          }}
        />
        <div
          className="body-text"
          style={{
            textAlign: 'center',
            marginTop: '4%',
            width: '70%',
          }}
        >
          {`Meet the bag that will keep you organized, looks great no matter what, and will be there for you, whether you’re caring for your newborn, or at the office taking care of business.

Sure, Mommin’ ain’t easy... but we like to think that a Jujube bag can make every day just a little bit easier.`}
        </div>
      </div>
    </div>
  )
}

export default Section1
