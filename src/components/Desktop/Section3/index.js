import React from 'react'
import Image from '../../Common/Image'
import './section.css'

class Section3 extends React.Component {
  constructor() {
    super()
    this.state = {
      active: 0,
    }
  }
  render() {
    let { section3_title, section3_detail } = this.props.data
    section3_detail = section3_detail.slice(0, section3_detail.length - 1)
    let grid = []
    section3_detail.forEach((data, index) => {
      section3_detail.forEach(({ title, body, image }, innerIndex) => {
        const active = this.state.active
        if (index === 0) {
          grid.push(
            <div
              onMouseEnter={() => this.setState({ active: innerIndex })}
              style={{
                display: 'flex',
                opacity: active === innerIndex ? 1 : 0.2,
                transition: '.3s all ease-in-out',
              }}
            >
              <div
                style={{
                  fontSize: 34,
                  letterSpacing: 0,
                  marginRight: '7%',
                  fontFamily: 'PlayfairDisplay',
                  fontWeight: 'bold',
                  transition: '.1s all ease-in-out',
                }}
              >
                {title}
              </div>
              {/* underscore mark */}
              {active === innerIndex && (
                <div
                  style={{
                    margin: '3% 0',
                    backgroundColor: '#F2AFA3',
                    width: '15%',
                    maxWidth: 80,
                    height: 2,
                    marginLeft: '10%',
                    margin: 'auto 0',
                  }}
                />
              )}
            </div>
          )
        }
        if (index === 1) {
          grid.push(
            <div onMouseEnter={() => this.setState({ active: innerIndex })}>
              <Image
                {...image}
                style={{
                  width: '100%',
                  opacity: active === innerIndex ? 1 : 0.2,
                  transition: '.3s all ease-in-out',
                }}
              />
            </div>
          )
        }
        if (index === 2) {
          grid.push(
            <div
              className="body-text"
              onMouseEnter={() => this.setState({ active: innerIndex })}
              style={{
                visibility: active === innerIndex ? 'visible' : 'hidden',
                opacity: active === innerIndex ? 1 : 0.2,
                transition: '.3s all ease-in-out',
              }}
            >
              {body}
            </div>
          )
        }
      })
    })
    return (
      <div style={{ margin: '8%' }}>
        <div
          style={{
            fontFamily: 'Montserrat',
            fontWeight: 400,
            fontSize: 18,
            color: '#3B3939',
            textAlign: 'center',
          }}
        >
          {section3_title}
        </div>
        <div
          id="section3_grid"
          style={{
            display: 'grid',
            width: '100%',
            marginTop: '3%',
            gridTemplateColumns: `repeat(${section3_detail.length}, 1fr)`,
            gridGap: '3vh 2.5vw',
            alignItems: 'center',
          }}
        >
          {grid}
        </div>
      </div>
    )
  }
}

export default Section3
