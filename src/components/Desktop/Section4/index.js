import React from 'react'
import { section4_background } from 'images'

const Section4 = ({ data }) => {
  const {
    section4_title,
    section4_image: {
      localFile: { publicURL },
    },
    section4_body,
  } = data
  return (
    <div style={{ position: 'relative', display: 'flex' }}>
      <img
        style={{
          width: '100%',
          zIndex: -1,
          position: 'absolute',
        }}
        src={section4_background}
      />
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          margin: '5% 16%',
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            width: '37%',
            justifyContent: 'center',
          }}
        >
          <div className="header-text">{section4_title}</div>
          {/* underscore mark */}
          <div
            style={{
              backgroundColor: '#F2AFA3',
              width: '15%',
              maxWidth: 80,
              height: 2,
              margin: '10% 0',
              alignSelf: 'flex-start',
            }}
          />
          <div className="body-text">{section4_body}</div>
        </div>
        <img
          style={{
            objectFit: 'contain',
            width: '35vw',
          }}
          src={publicURL}
        />
      </div>
    </div>
  )
}

export default Section4
