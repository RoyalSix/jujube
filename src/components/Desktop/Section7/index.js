import React from 'react'
import Image from '../../Common/Image'
import { section7_background_image } from 'images'

const Section7 = ({ data }) => {
  const { section7_reviews } = data
  return (
    <div
      style={{
        display: 'grid',
        gridTemplateColumns: `repeat(${section7_reviews.length}, 1fr)`,
        padding: '7%',
        gridGap: '0 2%',
        position: 'relative',
        overflowY: 'hidden',
      }}
    >
      <img
        style={{ position: 'absolute', top: '-22%', left: 0, zIndex: -1 }}
        alt="background"
        src={section7_background_image}
      />
      {section7_reviews.map(({ image, text, name }) => {
        return (
          <div
            style={{
              backgroundColor: 'white',
              boxShadow: '13px 12px 40px 0 rgba(0,0,0,0.06)',
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <Image {...image} style={{ width: '100%' }} />
            <div style={{ margin: '0 5%' }}>
              {/* underscore mark */}
              <div
                style={{
                  marginTop: '7%',
                  backgroundColor: '#F2AFA3',
                  width: '15%',
                  maxWidth: 80,
                  height: 2,
                }}
              />
              <div
                style={{
                  marginTop: '6%',
                  fontSize: 22,
                  color: 'black',
                  lineHeight: '27px',
                  letterSpacing: 0,
                  fontFamily: 'PlayfairDisplay',
                  fontWeight: 'bold',
                }}
              >
                {text}
              </div>
              <div
                style={{
                  fontSize: 14,
                  color: 'black',
                  lineHeight: '23px',
                  letterSpacing: 0,
                  margin: '3% 0 7%',
                  fontWeight: 400,
                }}
              >
                {name}
              </div>
            </div>
          </div>
        )
      })}
    </div>
  )
}

export default Section7
