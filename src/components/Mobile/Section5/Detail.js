import React from 'react'

const Detail = ({ image, title }) => {
  const {
    localFile: { publicURL },
  } = image
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <img
        style={{ width: '26vw', marginBottom: '7%', margin: '0 auto' }}
        src={publicURL}
      />
      <div
        style={{
          fontSize: 15,
          lineHeight: '23px',
          letterSpacing: 0,
          color: '#3B3939',
          textAlign: 'center',
          margin: '0 auto',
          marginTop: '10%',
          width: '65%',
          fontWeight: 400,
        }}
      >
        {title}
      </div>
    </div>
  )
}

export default Detail
