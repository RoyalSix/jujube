import React from 'react'
import Detail from './Detail.js'

const Section5 = ({ data }) => {
  const { section5_body, section5_title, section5_detail } = data
  return (
    <div style={{ backgroundColor: '#FBF3F2', padding: '15% 0' }}>
      <div style={{ width: '84%', margin: '0 auto' }}>
        <div
          style={{
            fontSize: 33,
            letterSpacing: 0,
            textAlign: 'center',
            fontFamily: 'PlayfairDisplay',
            fontWeight: 'bold',
          }}
        >
          {section5_title}
        </div>
        {/* underscore mark */}
        <div
          style={{
            margin: '6% auto',
            backgroundColor: '#F2AFA3',
            width: '15%',
            maxWidth: 80,
            height: 2,
          }}
        />
        <div
          style={{
            fontSize: 15,
            color: '#3B3939',
            letterSpacing: 0,
            lineHeight: '23px',
            textAlign: 'center',
            marginTop: '10%',
            fontWeight: 400,
          }}
        >
          {section5_body}
        </div>
        <div
          style={{
            display: 'grid',
            gridTemplateColumns: '1fr 1fr',
            marginTop: '15%',
            gridGap: '2em',
          }}
        >
          {section5_detail.map(detailData => (
            <Detail {...detailData} />
          ))}
        </div>
      </div>
    </div>
  )
}

export default Section5
