import React from 'react'
import { jujube_logo } from 'images'
import ExpandedMenu from './ExpandedMenu'
import './Menu.css'

class Menu extends React.Component {
  constructor() {
    super()
    this.state = {
      open: false,
    }
  }
  render() {
    const { data } = this.props
    return (
      <div
        style={{
          zIndex: 100,
          width: '100vw',
          position: 'fixed',
          backgroundColor: 'white',
          top: 0,
        }}
      >
        <div
          style={{
            margin: '0 7%',
            height: 70,
            backgroundColor: 'white',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <img
            style={{ cursor: 'pointer' }}
            onClick={() => (window.location.pathname = '/')}
            alt="jujube"
            src={jujube_logo}
          />
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            {data.map(({ title, url }) => (
              <a
                href={url}
                style={{
                  outline: 'none',
                  textDecoration: 'none',
                  height: 40,
                  backgroundColor: '#F2AFA3',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  cursor: 'pointer',
                  width: 115,
                  marginRight: '4vw',
                }}
              >
                <div
                  style={{
                    fontSize: 12,
                    color: 'white',
                    lineHeight: '20px',
                    textAlign: 'center',
                    letterSpacing: '1.5px',
                    fontFamily: 'Montserrat',
                  }}
                >
                  {title}
                </div>
              </a>
            ))}
            <div
              style={{
                display: 'flex',
                cursor: 'pointer',
                padding: '0 4%',
              }}
              onClick={() => this.setState({ open: !this.state.open })}
            >
              <div
                className={this.state.open ? 'open' : ''}
                id="hamburger-icon"
              >
                <span />
                <span />
                <span />
                <span />
              </div>
            </div>
          </div>
        </div>
        <ExpandedMenu open={this.state.open} />
      </div>
    )
  }
}

export default Menu
