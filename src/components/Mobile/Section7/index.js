import React from 'react'
import Image from '../../Common/Image'

const Section7 = ({ data }) => {
  const { section7_reviews } = data
  return (
    <div style={{ marginTop: '-6.3%', marginBottom: '20%' }}>
      {section7_reviews.map(({ image, text, name }) => {
        return (
          <div
            style={{
              backgroundColor: 'white',
              width: 'calc(100% - 50px)',
              margin: '0 auto',
              boxShadow: '13px 12px 40px 0 rgba(0,0,0,0.06)',
              display: 'flex',
              flexDirection: 'column',
              paddingBottom: '10%',
              marginTop: '9%',
            }}
          >
            <Image {...image} style={{ width: '100%' }} />
            <div style={{ margin: '0 5%' }}>
              {/* underscore mark */}
              <div
                style={{
                  marginTop: '6%',
                  backgroundColor: '#F2AFA3',
                  width: '15%',
                  maxWidth: 80,
                  height: 2,
                }}
              />
              <div
                style={{
                  marginTop: '6%',
                  fontSize: 18,
                  color: 'black',
                  lineHeight: '27px',
                  letterSpacing: 0,
                  fontFamily: 'PlayfairDisplay',
                  fontWeight: 'bold',
                }}
              >
                {text}
              </div>
              <div
                style={{
                  fontSize: 12,
                  color: 'black',
                  lineHeight: '23px',
                  letterSpacing: 0,
                  marginTop: '3%',
                  fontWeight: 400,
                }}
              >
                {name}
              </div>
            </div>
          </div>
        )
      })}
    </div>
  )
}

export default Section7
