import React from 'react'
import { contact_icon } from 'images'

const Subscribe = ({ data }) => {
  const { title, input_placeholder, button_text } = data
  return (
    <div style={{ marginBottom: '15%' }}>
      <div
        style={{
          color: '#6B6B6B',
          marginBottom: '7%',
          letterSpacing: '2px',
          fontSize: 15,
        }}
      >
        {title}
      </div>
      <div
        style={{
          position: 'relative',
          display: 'flex',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}
      >
        <div
          style={{
            borderBottom: '#BFBFBF 1px solid',
            paddingBottom: 5,
            position: 'relative',
          }}
        >
          <input
            placeholder={input_placeholder}
            style={{
              WebkitAppearance: 'none',
              paddingLeft: 0,
              WebkitTransform: 'translate3d(0, 0, 0)',
              backgroundColor: 'transparent',
              border: 'none',
              outline: 'none',
              color: '#F7F7F7',
              fontSize: 15,
              width: 'calc(100% - 40px)',
            }}
          />
          <img
            style={{
              top: '50%',
              transform: 'translate(-50%, -50%)',
              right: 0,
              height: 20,
              position: 'absolute',
            }}
            src={contact_icon}
          />
        </div>
        <div
          style={{
            marginLeft: '3%',
            backgroundColor: '#878787',
            borderRadius: 3,
            color: '#F7F7F7',
            width: '27vw',
            height: '7vh',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          {button_text}
        </div>
      </div>
    </div>
  )
}

export default Subscribe
