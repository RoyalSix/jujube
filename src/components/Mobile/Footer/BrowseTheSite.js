import React from 'react'

const BrowseTheSite = ({ data }) => {
  const { links, title } = data
  return (
    <div style={{ marginBottom: '12%' }}>
      <div
        style={{
          color: '#6B6B6B',
          marginBottom: '7%',
          letterSpacing: '2px',
          fontSize: 15,
        }}
      >
        {title}
      </div>
      {links.map(({ name, path }) => (
        <div
          onClick={() => (window.location.pathname = path)}
          style={{
            cursor: 'pointer',
            width: 'fit-content',
            color: '#BFBFBF',
            marginBottom: '5%',
            fontSize: 15,
          }}
        >
          {name}
        </div>
      ))}
    </div>
  )
}

export default BrowseTheSite
