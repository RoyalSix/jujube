import React from 'react'

const ContactSupport = ({ data }) => {
  const { title, phone_number, fax, name, email, time } = data
  return (
    <div style={{ marginBottom: '12%' }}>
      <div
        style={{
          color: '#6B6B6B',
          marginBottom: '7%',
          letterSpacing: '2px',
          fontSize: 15,
        }}
      >
        {title}
      </div>
      <div style={{ color: '#BFBFBF', marginBottom: '5%', fontSize: 15 }}>
        {time}
      </div>
      <div style={{ color: '#BFBFBF', marginBottom: '5%', fontSize: 15 }}>
        {phone_number}
      </div>
      <div style={{ color: '#BFBFBF', marginBottom: '5%', fontSize: 15 }}>
        {fax}
      </div>
      <div style={{ color: '#BFBFBF', marginBottom: '5%', fontSize: 15 }}>
        {email}
      </div>
      <div style={{ color: '#BFBFBF', marginBottom: '5%', fontSize: 15 }}>
        {name}
      </div>
    </div>
  )
}

export default ContactSupport
