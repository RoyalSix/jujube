import React from 'react'
import ContactSupport from './ContactSupport'
import BrowseTheSite from './BrowseTheSite'
import BestSellers from './BestSellers'
import Subscribe from './Subscribe'

const Footer = ({ data }) => {
  const {
    footer_subscribe,
    footer_contact_support,
    footer_browse_the_site,
    footer_copyright_notice,
    footer_shop_best_sellers,
  } = data
  return (
    <div
      style={{
        backgroundColor: '#252525',
        display: 'flex',
        justifyContent: 'center',
        padding: '10% 10% 8% 10%',
        flexDirection: 'column',
      }}
    >
      <ContactSupport data={footer_contact_support} />
      <BrowseTheSite data={footer_browse_the_site} />
      <BestSellers data={footer_shop_best_sellers} />
      <Subscribe data={footer_subscribe} />
      <div style={{ fontSize: 12, color: '#6D7071' }}>
        {footer_copyright_notice}
      </div>
    </div>
  )
}

export default Footer
