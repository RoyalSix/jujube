import React from 'react'

const InactiveFeature = ({ title, select }) => {
  return (
    <div
      onClick={select}
      style={{
        opacity: 0.2,
        color: 'black',
        letterSpacing: 0,
        fontSize: 42,
        marginTop: '10%',
        fontFamily: 'PlayfairDisplay',
        fontWeight: 'bold',
      }}
    >
      {title}
    </div>
  )
}

export default InactiveFeature
