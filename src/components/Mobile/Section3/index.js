import React from 'react'
import ActiveFeature from './ActiveFeature'

class Section3 extends React.Component {
  constructor() {
    super()
    this.state = {
      active: 0,
    }
    this.selectFeature = this.selectFeature.bind(this)
  }

  selectFeature(index) {
    this.setState({ active: index }, () => {
      const id = `#section-3-feature-${index}`
      $('html, body').animate({ scrollTop: $(id).offset().top - 100 }, 400)
    })
  }

  render() {
    const { section3_title, section3_detail } = this.props.data
    return (
      <div style={{ margin: '15% 8% 10% 8%' }}>
        <div
          style={{
            fontSize: 18,
            color: '#3B3939',
            letterSpacing: 0,
            textAlign: 'center',
          }}
        >
          {section3_title}
        </div>
        {section3_detail.map((data, index) => (
          <ActiveFeature {...data} />
        ))}
      </div>
    )
  }
}

export default Section3
