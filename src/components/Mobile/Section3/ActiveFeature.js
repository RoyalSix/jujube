import React from 'react'
import Image from '../../Common/Image'

const ActiveFeature = ({ image, body, title }) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          alignSelf: 'flex-start',
          width: '100%',
          marginTop: '5%',
        }}
      >
        <div
          style={{
            maxWidth: '60%',
            fontSize: 42,
            letterSpacing: 0,
            marginRight: '7%',
            fontFamily: 'PlayfairDisplay',
            fontWeight: 'bold',
          }}
        >
          {title}
        </div>
        {/* underscore mark */}
        <div
          style={{
            margin: '3% 0',
            backgroundColor: '#F2AFA3',
            width: '15%',
            maxWidth: 80,
            height: 2,
          }}
        />
      </div>
      <div
        style={{
          fontSize: 15,
          color: '#3B3939',
          lineHeight: '23px',
          letterSpacing: 0,
          marginTop: '5%',
          fontWeight: 400,
        }}
      >
        {body}
      </div>
      <Image {...image} style={{ width: '100%', margin: '7% 0' }} />
    </div>
  )
}

export default ActiveFeature
