import React from 'react'
import Image from '../../Common/Image'
import Button from '../../Common/Button'

const Section10 = ({ data }) => {
  const { section10_title, section10_products, section10_body } = data
  return (
    <div
      style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}
    >
      <div
        style={{
          fontSize: 33,
          color: 'black',
          letterSpacing: 0,
          marginTop: '15%',
          textAlign: 'center',
          width: '90%',
          fontFamily: 'PlayfairDisplay',
          fontWeight: 'bold',
        }}
      >
        {section10_title}
      </div>
      {/* underscore mark */}
      <div
        style={{
          margin: '6% 0',
          backgroundColor: '#F2AFA3',
          width: '15%',
          maxWidth: 80,
          height: 2,
        }}
      />
      <div
        style={{
          fontSize: 15,
          lineHeight: '23px',
          letterSpacing: 0,
          color: '#3B3939',
          marginTop: '2%',
          fontWeight: 400,
          width: '84%',
          textAlign: 'center',
          paddingBottom: '10%',
        }}
      >
        {section10_body}
      </div>
      {section10_products.map(({ image, title, subtitle, price, link }) => {
        return (
          <a
            href={link}
            style={{
              WebkitAppearance: 'none',
              outline: 'none',
              textDecoration: 'none',
              backgroundColor: 'white',
              width: 'calc(100% - 50px)',
              margin: '0 auto',
              boxShadow: '13px 12px 40px 0 rgba(0,0,0,0.06)',
              display: 'flex',
              flexDirection: 'column',
              paddingBottom: '5%',
              marginBottom: '9%',
            }}
          >
            <Image {...image} style={{ width: '100%' }} />
            <div style={{ margin: '0 5%' }}>
              {/* underscore mark */}
              <div
                style={{
                  marginTop: '7%',
                  backgroundColor: '#F2AFA3',
                  width: '15%',
                  maxWidth: 80,
                  height: 2,
                }}
              />
              <div
                style={{
                  marginTop: '6%',
                  fontSize: 18,
                  color: 'black',
                  lineHeight: '27px',
                  letterSpacing: 0,
                  fontFamily: 'PlayfairDisplay',
                  fontWeight: 'bold',
                }}
              >
                <span>{title}</span>
                <br />
                <span style={{ color: '#F2AFA3' }}>{subtitle}</span>
              </div>
              <div
                style={{
                  fontSize: 20,
                  color: 'black',
                  lineHeight: '23px',
                  letterSpacing: 0,
                  marginTop: '4%',
                  fontWeight: 400,
                }}
              >
                ${price}
              </div>
              <Button style={{ marginTop: '4%' }} label={'SHOP NOW'} />
            </div>
          </a>
        )
      })}
    </div>
  )
}

export default Section10
