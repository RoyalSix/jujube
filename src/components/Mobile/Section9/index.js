import React from 'react'
import Button from '../../Common/Button'
import Video from './Video'
import { section9_background_mobile } from 'images'

const Section9 = ({ data }) => {
  const {
    section9_video_mobile,
    section9_text,
    section9_title,
    section9_button_label,
    section9_button_alignment,
  } = data
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <Video
        src={section9_video_mobile.localFile.publicURL}
        style={{ width: '100vw' }}
      />
      <div style={{ position: 'relative', display: 'flex' }}>
        <img
          style={{ minHeight: '46vh', width: '100%', zIndex: -1 }}
          src={section9_background_mobile}
        />
        <div
          style={{
            width: '85%',
            margin: '0 auto',
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <div
            style={{
              fontSize: 33,
              color: 'black',
              textAlign: 'center',
              letterSpacing: 0,
              marginTop: '8%',
              textAlign: 'left',
              width: '90%',
              fontFamily: 'PlayfairDisplay',
              fontWeight: 'bold',
            }}
          >
            {section9_title}
          </div>
          {/* underscore mark */}
          <div
            style={{
              margin: '6% 0',
              backgroundColor: '#F2AFA3',
              width: '15%',
              maxWidth: 80,
              height: 2,
            }}
          />
          <div
            style={{
              fontSize: 15,
              lineHeight: '23px',
              letterSpacing: 0,
              color: '#3B3939',
              marginTop: '6%',
              fontWeight: 400,
            }}
          >
            {section9_text}
          </div>
          <Button
            style={{
              margin:
                section9_button_alignment === 'center'
                  ? '0 auto'
                  : section9_button_alignment === 'right'
                    ? '0 0 0 auto'
                    : '',
              marginTop: '13%',
            }}
            label={section9_button_label}
          />
        </div>
      </div>
    </div>
  )
}

export default Section9
