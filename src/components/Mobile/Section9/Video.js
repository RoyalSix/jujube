import React from 'react'

class Video extends React.Component {
  componentDidMount() {
    // // Change the second argument to your options:
    // // https://github.com/sampotts/plyr/#options
    // const player = new Plyr('#section9_video_player', {
    //   fullscreen: { enabled: false, fallback: true, iosNative: true },
    //   muted: true,
    //   clickToPlay: false,
    //   loop: {
    //     active: true,
    //   },
    // })
    // player.volume = 0
    // player.play()
  }
  render() {
    return (
      <video
        style={this.props.style}
        autoPlay
        muted
        loop
        playsinline
        // poster="https://media.giphy.com/media/5CIRqPOk0141W/giphy.gif"
      >
        <source src={this.props.src} type="video/mp4" />
      </video>
    )
  }
}

export default Video
