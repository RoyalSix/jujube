import React from 'react'
import Image from '../../Common/Image'
import { section4_background_mobile } from 'images'

const Section4 = ({ data }) => {
  const { section4_title, section4_image, section4_body } = data
  return (
    <div style={{ position: 'relative', display: 'flex', paddingBottom: '4%' }}>
      <img
        style={{
          width: '100%',
          zIndex: -1,
          marginTop: '63%',
          position: 'absolute',
        }}
        src={section4_background_mobile}
      />
      <div
        style={{
          width: '85%',
          margin: '0 auto',
          display: 'flex',
          flexDirection: 'column',
          marginTop: '25%',
        }}
      >
        <div
          style={{
            fontSize: 33,
            color: 'black',
            textAlign: 'center',
            letterSpacing: 0,
            marginTop: '8%',
            textAlign: 'left',
            width: '80%',
            fontFamily: 'PlayfairDisplay',
            fontWeight: 'bold',
          }}
        >
          {section4_title}
        </div>
        {/* underscore mark */}
        <div
          style={{
            margin: '6% 0',
            backgroundColor: '#F2AFA3',
            width: '15%',
            maxWidth: 80,
            height: 2,
          }}
        />
        <div
          style={{
            fontSize: 15,
            lineHeight: '23px',
            letterSpacing: 0,
            color: '#3B3939',
            marginTop: '4%',
            fontWeight: 400,
          }}
        >
          {section4_body}
        </div>
        <Image
          {...section4_image}
          style={{
            objectFit: 'contain',
            width: '100%',
            marginTop: '14%',
          }}
        />
      </div>
    </div>
  )
}

export default Section4
