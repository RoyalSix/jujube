import React from 'react'

class Video extends React.Component {
  componentDidMount() {
    // Change the second argument to your options:
    // https://github.com/sampotts/plyr/#options
    const player = new Plyr('#hero_player', {
      fullscreen: { enabled: true },
    })
    const _this = this;
    player.on('exitfullscreen', function(event) {
      const instance = event.detail.plyr
      _this.props.close()
      window.setTimeout(() => {
        instance.stop()
      }, 200)
    })
  }
  // UNSAFE_componentWillReceiveProps(nextProps) {
  //   if (nextProps.open && !this.props.open) {

  //   }
  // }
  render() {
    return (
      <div>
        <video fullscreen autoPlay id="hero_player">
          <source
            src="https://multicdn.synq.fm/projects/bb/56/bb56f28429b942c08dc5128e4b7ba48c/derivatives/videos/71/43/71439ccd73c74ecc8bbab7abd3bb98bc/mp4_1080/71439ccd73c74ecc8bbab7abd3bb98bc_mp4_1080.mp4"
            type="video/mp4"
          />
        </video>
      </div>
    )
  }
}

export default Video
