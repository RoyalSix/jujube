import React from 'react'
import Video from './Video'
import Image from '../../Common/Image'
import { play_button } from 'images'

class Hero extends React.Component {
  constructor() {
    super()
    this.state = {
      open: false,
    }
    this.close = this.close.bind(this)
  }

  close() {
    this.setState({
      open: false,
    })
    this.props.showVideo(false)
  }

  render() {
    const { data } = this.props
    const { hero_background_image_mobile } = data
    return (
      <div
        style={{
          width: '100%',
        }}
      >
        {this.state.open && <Video close={this.close} />}
        <div
          style={{
            position: 'relative',
          }}
          onClick={() => {
            this.props.showVideo(true)
            this.setState({
              open: true,
            })
          }}
        >
          <img
            style={{
              filter: 'drop-shadow(0px 0px 12px rgba(0,0,0,0.20))',
              zIndex: 1,
              height: '11vw',
              minHeight: 120,
              position: 'absolute',
              top: '50%',
              left: '50%',
              transform: 'translate(-50%, -50%)',
            }}
            src={play_button}
          />
          {/* <div
            style={{
              fontFamily: 'PlayfairDisplay',
              position: 'absolute',
              top: '20%',
              left: '30%',
              transform: 'translate(-50%, -50%)',
              zIndex: 1,
              color: 'white',
              fontSize: '3em',
            }}
          >
            Mommin' Ain't Easy
          </div> */}
          <div>
            <Image
              {...hero_background_image_mobile}
              style={{
                outline: 'none',
                width: '100%',
                objectFit: 'contain',
              }}
            />
          </div>
        </div>
      </div>
    )
  }
}
export default Hero
