import React from 'react'
import Swiper from 'react-slick'
import Image from '../../Common/Image'

class Section6 extends React.Component {
  componentDidMount() {
    window.addEventListener('touchstart', this.touchStart)
    window.addEventListener('touchmove', this.preventTouch, { passive: false })
  }

  componentWillUnmount() {
    window.removeEventListener('touchstart', this.touchStart)
    window.removeEventListener('touchmove', this.preventTouch, {
      passive: false,
    })
  }

  touchStart(e) {
    this.firstClientX = e.touches[0].clientX
    this.firstClientY = e.touches[0].clientY
  }

  preventTouch(e) {
    const minValue = 5 // threshold

    this.clientX = e.touches[0].clientX - this.firstClientX
    this.clientY = e.touches[0].clientY - this.firstClientY

    // Vertical scrolling does not work when you start swiping horizontally.
    if (Math.abs(this.clientX) > minValue) {
      e.preventDefault()
      e.returnValue = false
      return false
    }
  }
  render() {
    const {
      data: { section6_carousel },
    } = this.props
    var settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <div style={{ display: 'none' }} />,
      prevArrow: <div style={{ display: 'none' }} />,
    }
    return (
      <Swiper {...settings}>
        {section6_carousel.map(({ image, title, body }, index) => {
          return (
            <div>
              <Image {...image} style={{ width: '100vw', height: '70vh' }} />
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  textAlign: 'left',
                  position: 'absolute',
                  bottom: 0,
                  margin: '6% 2.5%',
                  width: '27%',
                }}
              >
                <div
                  style={{
                    fontFamily: 'Montserrat',
                    fontWeight: 'bold',
                    fontSize: 14,
                    color: '#F2AFA3',
                    lineHeight: '23px',
                  }}
                >
                  0{index + 1} — 0{section6_carousel.length}
                </div>
                <div
                  style={{
                    fontSize: 33,
                    color: 'white',
                    letterSpacing: 0,
                    fontFamily: 'PlayfairDisplay',
                    fontWeight: 'bold',
                  }}
                >
                  {title}
                </div>
                {/* underscore mark */}
                <div
                  style={{
                    backgroundColor: '#F2AFA3',
                    width: '15%',
                    maxWidth: 80,
                    height: 2,
                    margin: '5% 0px 8%',
                    alignSelf: 'flex-start',
                  }}
                />
                <div
                  style={{
                    fontSize: 15,
                    lineHeight: '23px',
                    letterSpacing: 0,
                    color: 'white',
                    fontWeight: 400,
                  }}
                >
                  {body}
                </div>
              </div>
            </div>
          )
        })}
      </Swiper>
    )
  }
}

export default Section6
