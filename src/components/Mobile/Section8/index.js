import React from 'react'
import { section8_background_mobile } from 'images'

const Section8 = ({ data }) => {
  const { section8_title, section8_text } = data
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        flexDirection: 'column'
      }}
    >
      <img
        style={{ width: '100%', top: 0, zIndex: -1 }}
        src={section8_background_mobile}
      />
      <div
        style={{
          width: '100%',
          margin: '0 auto',
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          display: 'flex',
          flexDirection: 'column',
          alignItems:'center'
        }}
      >
        <div
          style={{
            fontSize: 33,
            color: 'black',
            textAlign: 'center',
            letterSpacing: 0,
            marginTop: '8%',
            textAlign: 'center',
            width: '80%',
            fontFamily: 'PlayfairDisplay',
            fontWeight: 'bold',
          }}
        >
          {section8_title}
        </div>
        {/* underscore mark */}
        <div
          style={{
            marginTop: '5%',
            backgroundColor: '#F2AFA3',
            width: '15%',
            maxWidth: 80,
            height: 2,
          }}
        />
        <div
          style={{
            color: '#3B3939',
            fontSize: 15,
            lineHeight: '23px',
            textAlign: 'center',
            width: '70%',
            marginTop: '9%',
            fontWeight: 400,
          }}
        >
          {section8_text}
        </div>
      </div>
    </div>
  )
}

export default Section8
