import React from 'react'
import { section1_paint_mark } from 'images'

const Section1 = ({ data }) => {
  const { section1_title, section1_body } = data
  return (
    <div style={{ position: 'relative', zIndex: -1 }}>
      <img
        alt="mark"
        src={section1_paint_mark}
        style={{
          opacity: 0.5,
          height: '70%',
          position: 'absolute',
          top: '-23%',
        }}
      />
      <div
        style={{
          margin: '10% 7%',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <div
          style={{
            color: 'black',
            fontSize: 33,
            letterSpacing: '0',
            alignSelf: 'flex-start',
            marginBottom: '5%',
            fontFamily: 'PlayfairDisplay',
            fontWeight: 'bold',
          }}
        >
          {section1_title}
        </div>
        {/* underscore mark */}
        {/* underscore mark */}
        <div
          style={{
            alignSelf: 'flex-start',
            backgroundColor: '#F2AFA3',
            width: '15%',
            maxWidth: 80,
            height: 2,
          }}
        />
        <div
          style={{
            textAlign: 'left',
            color: '#3B3939',
            fontSize: 15,
            letterSpacing: 0,
            lineHeight: '23px',
            marginTop: '8%',
            fontWeight: 400,
          }}
        >
          {`Meet the bag that will keep you organized, looks great no matter what, and will be there for you, whether you’re caring for your newborn, or at the office taking care of business.

Sure, Mommin’ ain’t easy... but we like to think that a Jujube bag can make every day just a little bit easier.`}
        </div>
      </div>
    </div>
  )
}

export default Section1
