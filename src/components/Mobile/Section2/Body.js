import React from 'react'

const Body = ({ section2_body }) => {
  return (
    <div
      style={{
        fontSize: 15,
        lineHeight: '23px',
        letterSpacing: 0,
        color: '#3B3939',
        fontWeight: 400,
        width: '84%',
        margin: '0 auto',
        textAlign: 'center',
        paddingBottom: '9%',
      }}
    >
      {section2_body}
    </div>
  )
}

export default Body
