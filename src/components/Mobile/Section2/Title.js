import React from 'react'
const title = 'Made With Mom In Mind'

const Title = ({ section2_title }) => {
  return (
    <div
      style={{
        fontSize: 33,
        letterSpacing: 0,
        textAlign: 'center',
        color: 'black',
        padding: '6% 0',
        margin: '0 10%',
        fontFamily: 'PlayfairDisplay',
        fontWeight: 'bold',
      }}
    >
      {section2_title}
    </div>
  )
}

export default Title
