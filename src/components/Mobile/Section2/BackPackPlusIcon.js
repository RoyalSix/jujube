import React from 'react'
import { backpack_add_icon } from 'images'

class BackPackPlusIcon extends React.Component {
  componentDidMount() {
    const { index } = this.props
    if (
      $(`#backpack-feature-text-${index}`) &&
      $(`#backpack-feature-text-${index}`).offset().left +
        $(`#backpack-feature-text-${index}`).width() >
        window.innerWidth
    ) {
      let widthSetterInterval = setInterval(widthChanger(index), 100)
      function widthChanger(index) {
        if (
          $(`#backpack-feature-text-${index}`).offset().left +
            $(`#backpack-feature-text-${index}`).width() >
          window.innerWidth
        ) {
          //width is too big
          const extraWidth =
            $(`#backpack-feature-text-${index}`).offset().left +
            $(`#backpack-feature-text-${index}`).width() -
            window.innerWidth
          const newWidth =
            $(`#backpack-feature-text-${index}`).width() - extraWidth - 5
          $(`#backpack-feature-text-${index}`).width(
            `${(newWidth / window.innerWidth) * 100}vw`
          )
        } else {
          clearInterval(widthSetterInterval)
        }
      }
    }
  }
  render() {
    const { x, y, body, selected, select, index, title } = this.props
    const transformStyle = selected
      ? {
          transform: 'rotate(135deg)',
        }
      : {}

    const textBoxStyle = selected
      ? {
          opacity: 1,
          visibility: 'visible',
        }
      : {
          opacity: 0,
          visibility: 'hidden',
        }
    return (
      <div
        style={{
          zIndex: selected ? 2 : 1,
          height: 50,
          width: 50,
          position: 'absolute',
          top: `${y}%`,
          left: `${x}%`,
          display: 'flex',
          cursor: 'pointer',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <img
          onClick={select}
          style={{
            zIndex: 2,
            objectFit: 'cover',
            marginLeft: '.3em',
            marginTop: '.2em',
            transition: 'transform .1s ease-in-out',
            ...transformStyle,
          }}
          src={backpack_add_icon}
        />
        <div
          id={`backpack-feature-text-${index}`}
          style={{
            backgroundColor: 'white',
            borderRadius: 10,
            maxHeight: 220,
            height: '33vh',
            maxWidth: 243,
            width: '65vw',
            position: 'absolute',
            transition: 'opacity .15s ease-in',
            transitionDelay: '.1s',
            top: '50%',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            ...textBoxStyle,
            zIndex: 1,
          }}
        >
          <div
            style={{
              textAlign: 'center',
              fontSize: 20,
              letterSpacing: 0,
              color: 'black',
              fontFamily: 'PlayfairDisplay',
              fontWeight: 'bold',
              marginBottom: '3%',
            }}
          >
            {title}
          </div>
          <div
            style={{
              textAlign: 'center',
              lineHeight: '23px',
              fontSize: 14,
              letterSpacing: 0,
              color: '#3B3939',
              fontWeight: 400,
              margin: '0 10%',
            }}
          >
            {body}
          </div>
        </div>
      </div>
    )
  }
}

export default BackPackPlusIcon
