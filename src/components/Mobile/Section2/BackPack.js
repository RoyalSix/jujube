import React from 'react'
import BackPackPlusIcon from './BackPackPlusIcon'
import Image from '../../Common/Image'

class BackPack extends React.Component {
  constructor() {
    super()
    this.state = {
      selected: {},
      closeFromTouchEvent: false,
    }
    this.showFeature = this.showFeature.bind(this)
  }

  componentDidMount() {
    window.addEventListener('touchend', () => {
      const somethingSelected = Object.keys(this.state.selected).filter(el =>
        Boolean(this.state.selected[el])
      ).length
      if (somethingSelected) {
        this.setState({
          selected: {},
          closeFromTouchEvent: true,
        })
      }
    })
  }

  showFeature(index, on = false) {
    if (on) {
      this.setState(
        {
          selected: {
            [index]: !this.state.selected[index],
          },
          closeFromTouchEvent: false,
        },
        () => {
          var $window = $(window),
            $element = $(`#backpack-feature-text-${index}`),
            elementTop = $element.offset().top,
            elementHeight = $element.height(),
            viewportHeight = $window.height(),
            scrollIt = elementTop - (viewportHeight - elementHeight) / 2
          $('html, body').animate({ scrollTop: scrollIt }, 400)
        }
      )
    }
  }

  render() {
    const { section2_image, section2_detail } = this.props
    return (
      <div
        style={{
          position: 'relative',
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        {section2_detail.map(
          (
            { detail_x_position, detail_y_position, detail_body, detail_title },
            index
          ) => (
            <BackPackPlusIcon
              select={() => {
                this.setState({ closeFromTouchEvent: false })
                this.showFeature(index, !this.state.closeFromTouchEvent)
              }}
              selected={this.state.selected[index]}
              x={detail_x_position}
              y={detail_y_position}
              body={detail_body}
              title={detail_title}
              index={index}
            />
          )
        )}
        <Image
          {...section2_image}
          style={{
            width: '70%',
            height: '100%',
            zIndex: 0,
          }}
          imgStyle={{
            width: '100%',
            height: '100%',
          }}
        />
      </div>
    )
  }
}

export default BackPack
