import React from 'react'
import './Button.css'

const Button = ({ label, style, href, onClick }) => {
  return (
    <div
      onClick={href ? () => (window.location = href) : onClick}
      id="standard-button"
      style={{
        cursor: 'pointer',
        color: 'white',
        backgroundColor: '#F2AFA3',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '7.5vh',
        width: '23vw',
        letterSpacing: '2px',
        fontSize: 16,
        lineHeight: '20px',
        maxHeight: 60,
        maxWidth: '27vh',
        padding: '0 15px',
        minWidth: 'fit-content',
        ...style,
      }}
    >
      {label}
    </div>
  )
}

export default Button
