import React from 'react'
import Img from 'gatsby-image'

const Image = ({ source_url, localFile = {}, ...props }) => {
  if (localFile) {
    if (localFile.childImageSharp) {
      if (localFile.childImageSharp.fluid) {
        return <Img {...props} fluid={localFile.childImageSharp.fluid} />
      } else if (localFile.childImageSharp.fixed) {
        return <Img {...props} fixed={localFile.childImageSharp.fixed} />
      }
    } else if (localFile.publicURL) {
      return <img src={localFile.publicURL} {...props} />
    }
  } else if (source_url) {
    return <img src={source_url} {...props} />
  } else {
    return <div />
  }
  return content
}

export default Image
