export function DataSelector(state) {
  var self = {
    getAllDataByType: type => {
      for (var key in state) {
        if (key.includes(`${type}_data`)) {
          if (!state[key].edges) return state[key];
          return state[key].edges.map(({ node }) => node)
        }
      }
    },
  }
  return self
}
